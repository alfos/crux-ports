CRUX Linux ports repository
===========================

This repository contains the Pkgbuild files for each and every CRUX Linux package, along with the required patches and scripts, if any.
