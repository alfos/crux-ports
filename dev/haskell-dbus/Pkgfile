# Description: A client library for the D-Bus IPS system
# URL: https://github.com/rblaze/haskell-dbus
# Maintainer: Alexander Osetrov, osetrovaf at gmail dot com
# Depends on: ghc haskell-cereal haskell-conduit haskell-exceptions haskell-lens haskell-network haskell-random haskell-split haskell-th-lift haskell-vector haskell-xml-conduit haskell-xml-types

name=haskell-dbus
version=1.2.14
release=1
source=(https://hackage.haskell.org/packages/archive/${name#haskell-}/$version/${name#haskell-}-$version.tar.gz)

build() {
	cd ${name#haskell-}-$version

	GHC_VERSION=$(ghc -V |awk '{print $NF}')

	ghc-pkg recache

	runhaskell Setup configure -O \
		--prefix=/usr \
		--enable-shared \
		--enable-library-profiling \
		--libdir=/usr/lib \
		--libsubdir=ghc-${GHC_VERSION}/${name#haskell-}-${version} \
		--docdir=/usr/doc/$name-$version

	runhaskell Setup build
	runhaskell Setup copy --destdir=$PKG
	runhaskell Setup register --gen-pkg-config

	pkg_id="$(grep '^id: ' ${name#haskell-}-${version}.conf |cut -d' ' -f2)"
	mkdir -p $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d
	mv ${name#haskell-}-${version}.conf \
	   $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d/${pkg_id}.conf

	rm -rf $PKG/usr/doc
}
