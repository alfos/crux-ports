# Description: Type classes for mapping, folding, and traversing monomorphic containers
# URL: https://github.com/snoyberg/mono-traversable
# Maintainer: Alexander Osetrov, osetrovaf at gmail dot com
# Depends on: ghc haskell-hashable haskell-split haskell-unordered-containers haskell-vector haskell-vector-algorithms

name=haskell-mono-traversable
version=1.0.15.1
release=1
source=(https://hackage.haskell.org/packages/archive/${name#haskell-}/$version/${name#haskell-}-$version.tar.gz)

build() {
	cd ${name#haskell-}-$version
	
	GHC_VERSION=$(ghc -V |awk '{print $NF}')
	
	ghc-pkg recache
	
	runhaskell Setup configure -O \
		--prefix=/usr \
		--enable-shared \
		--enable-library-profiling \
		--libdir=/usr/lib \
		--libsubdir=ghc-${GHC_VERSION}/${name#haskell-}-${version} \
		--docdir=/usr/doc/$name-$version

	runhaskell Setup build
	runhaskell Setup copy --destdir=$PKG
	runhaskell Setup register --gen-pkg-config

	pkg_id="$(grep '^id: ' ${name#haskell-}-${version}.conf |cut -d' ' -f2)"
	mkdir -p $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d
	mv ${name#haskell-}-${version}.conf \
	   $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d/${pkg_id}.conf

	rm -rf $PKG/usr/doc
}
