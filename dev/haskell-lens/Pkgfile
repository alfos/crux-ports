# Description: Lenses, Folds and Traversals
# URL: https://github.com/ekmett/lens
# Maintainer: Alexander Osetrov, osetrovaf at gmail dot com
# Depends on: ghc haskell-base-orphans haskell-bifunctors haskell-call-stack haskell-comonad haskell-contravariant haskell-distributive haskell-free haskell-hashable haskell-kan-extensions haskell-exceptions haskell-parallel haskell-profunctors haskell-reflection haskell-semigroupoids haskell-tagged haskell-th-abstraction haskell-transformers-compat haskell-unordered-containers haskell-vector

name=haskell-lens
version=4.19.2
release=1
source=(https://hackage.haskell.org/packages/archive/${name#haskell-}/$version/${name#haskell-}-$version.tar.gz)

build() {
	cd ${name#haskell-}-$version

	GHC_VERSION=$(ghc -V |awk '{print $NF}')
	
	ghc-pkg recache

	runhaskell Setup configure -O \
		--prefix=/usr \
		--enable-shared \
		--enable-library-profiling \
		--libdir=/usr/lib \
		--libsubdir=ghc-${GHC_VERSION}/${name#haskell-}-${version} \
		--docdir=/usr/doc/$name-$version \
		-f-test-doctests -f-test-hunit -f-test-properties \
		-f-test-templates

	runhaskell Setup build
	runhaskell Setup copy --destdir=$PKG
	runhaskell Setup register --gen-pkg-config

	pkg_id="$(grep '^id: ' ${name#haskell-}-${version}.conf |cut -d' ' -f2)"
	mkdir -p $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d
	mv ${name#haskell-}-${version}.conf \
	   $PKG/usr/lib/ghc-${GHC_VERSION}/package.conf.d/${pkg_id}.conf

	rm -rf $PKG/usr/doc
}
